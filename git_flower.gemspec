# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "git_flower/version"

Gem::Specification.new do |spec|
  spec.name          = "git_flower"
  spec.version       = GitFlower::VERSION
  spec.authors       = ["Alex Wilkinson"]
  spec.email         = ["alex.wilkinson@me.com"]

  spec.summary       = %q{A CLI for starting git-flow features and hotfixes on PivotalTracker.}
  spec.description   = %q{This library starts features and hotfixes using the git-flow branching model and creates corresponding features and bugs on PivotalTracker using the Pivotal API.}
  spec.homepage      = "https://gitlab.com/alexwilkinson/git-flower"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.required_ruby_version = '>= 2.2.2'

  spec.add_development_dependency "bundler", "~> 1.15"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "byebug"
  spec.add_development_dependency "simplecov", "~>0.14"
  spec.add_development_dependency "fakefs", "~>0.11"

  spec.add_dependency "git", "~> 1.3.0"
  spec.add_dependency "thor", "~> 0.19.4"
  spec.add_dependency "activesupport", "~> 5.1.2"
  spec.add_dependency "faraday", "~> 0.9.0"
  spec.add_dependency "tracker_api", "0.2.9"
end
