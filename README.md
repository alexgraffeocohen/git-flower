# GitFlower

[![build status](https://gitlab.com/alexwilkinson/git-flower/badges/master/build.svg)](https://gitlab.com/alexwilkinson/git-flower/commits/master)
[![coverage report](https://gitlab.com/alexwilkinson/git-flower/badges/master/coverage.svg)](https://gitlab.com/alexwilkinson/git-flower/commits/master)
[![Gem Version](https://badge.fury.io/rb/git_flower.svg)](https://badge.fury.io/rb/git_flower)

GitFlower is a command-line utility that starts features and hotfixes using the git-flow branching model and creates corresponding features and bugs on PivotalTracker using the Pivotal API. Link tickets in Pivotal with your git-flow branches without leaving the comfort of the command line!

While GitFlower only supports Pivotal at the moment, the plan is to add support for all popular project management systems. Once I have developed an adapter system for easily working with multiple APIs, I will publish my priority list on this README.

If I'm not currently planning on adding your favorite PM tool, or it's not happening fast enough, feel free to contribute!

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'git_flower'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install git_flower

You will have to export your Pivotal API Key and a Project ID from your
environment. Here's an example of how to do so in ZSH:

```zsh
export PIVOTAL_TOKEN="TOKEN HERE"
export PIVOTAL_PROJECT_ID="some_number_here"
```

Make sure you have run `git flow init` on the project you want to use this gem with! Check out the [gitflow wiki](https://github.com/nvie/gitflow/wiki/Installation) for up to date installation instructions if you don't have git-flow yet.

## Usage

The `git-flower create` commands will create a Pivotal story for your feature or hotfix, start a git-flow branch off of develop, and check out that branch. All you need to do is pass a story name, and it will set a titleized version as the Pivotal ticket name and a slugified version as the branch name.

The commands won't run under the following conditions:

  * You have added but uncommitted git changes.
  * Local develop is not up to date with origin.

By default, it will use `$USER` as the owner of the Pivotal story. For Pivotal to properly assign the owner, the name of your Pivotal user needs to match`$USER`.

### Feature

To create a new feature, with optional labels for the Pivotal feature, run the
following:

    $ git-flower feature create [NAME] [--labels=one two three]

So if you wanted to create a Pivotal feature named "Save All The Cats" with the labels `cat` and `life-goal`, you could execute the following:

    $ git-flower feature create "Save All The Cats" --labels cat life-goal

And you will be checked out to the following branch, where the number
represents the Pivotal Story ID.

```
feature/1234-save-all-the-cats
```

Your Pivotal Feature would look something like this:

![Pivotal Feature Example](https://farm5.staticflickr.com/4279/35816647836_e20c3d465d_k.jpg)

### Hotfix

To create a new hotfix, with optional labels for the Pivotal bug, run the
following:

    $ git-flower hotfix create [NAME] [--labels=one two three]

So if you wanted to create a Pivotal bug named "Missing Cat" with
the labels `cat` and `nightmare`, you could execute the following:

    $ git-flower hotfix create "Missing Cat" --labels cat nightmare

And you will be checked out to the following branch, where the number
represents the Pivotal Story ID.

```
hotfix/5678-missing-cat
```

Your Pivotal Bug would look something like this:

![Pivotal Bug Example](https://farm5.staticflickr.com/4207/35688101172_2cce13f24b_k.jpg)

### Hitch Integration

If you're a pair programmer, and would like to set joint assignees on your
Pivotal tickets using [Hitch](https://github.com/therubymug/hitch), make sure you have the library configured and have a `.hitchrc` file in your home directory.

If all is set up correctly, GitFlower will detect who you are hitched with and
add both as story owners. If you are not hitched with anyone, it will default
to `$USER`.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/alexwilkinson/git-flower. If there isn't already an open Issue attempting to address your bug or feature request, please feel free to open one and I'll follow up within 24 hours.

 This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT
License](http://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the GitFlower project’s code base and issue trackers is expected to follow the [code of conduct](https://gitlab.com/alexwilkinson/git-flower/blob/master/CODE_OF_CONDUCT.md).
