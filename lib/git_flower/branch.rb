module GitFlower
  class Branch
    def initialize(name:, id:, type:)
      @story_name = name
      @story_id = id
      @type = type
    end

    def name
      Shellwords.shellescape("#{type}/#{story_id}-#{story_name.parameterize}")
    end

    private

    attr_reader :story_name, :story_id, :type
  end
end
