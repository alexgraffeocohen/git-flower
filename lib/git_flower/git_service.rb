require 'git'
require_relative 'branch'
require_relative 'git_repository'

class GitError < StandardError; end
class GitFlowError < StandardError; end

module GitFlower
  class GitService
    def initialize(repository:)
      @repository = repository
    end

    def start_branch(name:, id:, type:)
      branch_name = GitFlower::Branch.new(name: name, id: id, type: type).name
      repository.checkout_branch(branch_name)
    end

    def validate_for_hotfix!
      puts "Checking for commited files"
      if repository.has_added_files?
        raise GitError, "Please commit or stash all added files."
      end

      puts "Checking if master and develop are up to date"
      if !repository.master_up_to_date? || !repository.develop_up_to_date?
        raise GitError, "Please pull --rebase master and develop"
      end

      puts "Checking for existing hotfix branch"
      if repository.has_existing_branch_name?("hotfix")
        raise GitFlowError, "You already have an existing hotfix branch"
      end
    end

    def validate_for_feature!
      puts "Checking for commited files"
      if repository.has_added_files?
        raise GitError, "Please commit or stash all added files."
      end

      puts "Checking if develop is up to date"
      if !repository.develop_up_to_date?
        raise GitError, "Please pull --rebase develop"
      end
    end

    private

    attr_reader :repository
  end
end
