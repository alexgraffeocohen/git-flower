require 'git'

module GitFlower
  class GitRepository
    def initialize(path_to_repo)
      @repo = find_git_repo(path_to_repo)
    end

    def find_git_repo(initial_dir)
      if Dir.exist?('.git')
        Git.open(initial_dir)
      else
        raise GitError, "Please navigate to the root of your git repository to use this gem."
      end
    end

    def checkout_branch(branch_name)
      @repo.branch(branch_name).checkout
    end

    def has_added_files?
      !@repo.status.added.empty?
    end

    def master_up_to_date?
      branch_up_to_date?('master')
    end

    def develop_up_to_date?
      branch_up_to_date?('develop')
    end

    def has_existing_branch_name?(branch_name)
      !@repo.branches.local.map(&:name).select { |name| name =~ /^#{branch_name}.*/ }.empty?
    end

    def branch_up_to_date?(branch_name)
      @repo.revparse(branch_name) == @repo.revparse("origin/#{branch_name}")
    end
  end
end
