require 'shellwords'
require 'yaml'
require 'active_support'
require_relative 'git_service'
require_relative 'git_repository'
require_relative 'pivotal_project'

module GitFlower
  class Story
    def initialize(story_name:, labels:)
      @story_name = story_name
      @labels = Array(labels)
    end

    def create_story!(type)
      if type.nil? || !["feature", "hotfix"].include?(type)
        raise ArgumentError, "You need to specify that your story is a feature or a hotfix"
      end

      story = pivotal_project.create_story(type,
                                           name: story_name.titleize,
                                           labels: labels,
                                           owner_usernames: derive_story_owners)

      puts "Your pivotal story is https://www.pivotaltracker.com/story/show/#{story.id}"
      puts "Creating #{type} branch for #{story_name}"

      GitService.
        new(repository: GitFlower::GitRepository.new(Dir.pwd)).
        start_branch(name: story_name, id: story.id, type: type)
    end

    private

    attr_reader :story_name, :labels

    def validate_environment_and_arguments!
      if env_variable_undefined?(story_name)
        raise ArgumentError, "please pass a pivotal story name"
      end

      if env_variable_undefined?(project_id)
        raise ArgumentError, "You need to populate the PIVOTAL_PROJECT_ID environment variable"
      end

      if env_variable_undefined?(pivotal_token)
        raise ArgumentError, "You need to populate the PIVOTAL_TOKEN environment variable"
      end
    end

    def env_variable_undefined?(variable)
      variable.nil? || variable.empty?
    end

    def pivotal_project
      PivotalProject.new(pivotal_token, project_id)
    end

    def pivotal_token
      ENV['PIVOTAL_TOKEN']
    end

    def project_id
      ENV['PIVOTAL_PROJECT_ID']
    end

    def derive_story_owners
      begin
        hitch_config = YAML.load_file("#{ENV['HOME']}/.hitchrc")
        if hitch_config[:current_pair].empty?
          Array(ENV['USER'])
        else
          hitch_config[:current_pair]
        end
      rescue
        Array(ENV['USER'])
      end
    end
  end
end
