require 'thor'
require_relative './feature-cli.rb'
require_relative './hotfix-cli.rb'

module GitFlower
  class CLI < Thor
    desc "feature SUBCOMMAND ...ARGS", "manage git-flow features"
    subcommand "feature", GitFlower::FeatureCLI

    desc "hotfix SUBCOMMAND ...ARGS", "manage git-flow hotfixes"
    subcommand "hotfix", GitFlower::HotfixCLI
  end
end
