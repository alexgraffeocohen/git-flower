require "spec_helper"
require "git"

RSpec.describe GitFlower::GitService do
  let(:git_repository) {
    instance_double("GitFlower::GitRepository")
  }
  let(:git_service) {
    GitFlower::GitService.new(repository: git_repository)
  }

  context "#start_branch" do
    it "calls out to Git library to create and checkout branch using arguments" do
      story_name = "Save The Cats"
      id = 1
      branch_name = GitFlower::Branch.new(
        name: story_name,
        id: id,
        type: "feature"
      ).name

      expect(git_repository).
        to receive(:checkout_branch).
        with(branch_name)

      git_service.start_branch(
        name: story_name,
        id: id,
        type: "feature"
      )
    end
  end

  context "#validate_for_feature!" do
    it "prints status statements" do
      allow(git_repository).to receive(:has_added_files?).and_return(false)
      allow(git_repository).to receive(:develop_up_to_date?).and_return(true)

      expect { git_service.validate_for_feature! }.
        to output("Checking for commited files\nChecking if develop is up to date\n").to_stdout
    end

    it "does not raise an error when repository state is valid" do
      allow(git_repository).to receive(:has_added_files?).and_return(false)
      allow(git_repository).to receive(:develop_up_to_date?).and_return(true)

      expect { git_service.validate_for_feature! }.to_not raise_error
    end

    it "raises a GitError when there are staged files" do
      allow(git_repository).to receive(:develop_up_to_date?).and_return(true)

      expect(git_repository).to receive(:has_added_files?).and_return(true)

      expect { git_service.validate_for_feature! }.to raise_error(GitError)
    end

    it "raises a GitError when develop is not up to date with origin" do
      allow(git_repository).to receive(:has_added_files?).and_return(false)

      expect(git_repository).to receive(:develop_up_to_date?).and_return(false)

      expect { git_service.validate_for_feature! }.to raise_error(GitError)
    end
  end

  context "#validate_for_hotfix!" do
    it "prints status statements" do
      allow(git_repository).to receive(:has_added_files?).and_return(false)
      allow(git_repository).to receive(:develop_up_to_date?).and_return(true)
      allow(git_repository).to receive(:master_up_to_date?).and_return(true)
      allow(git_repository).to receive(:has_existing_branch_name?).and_return(false)

      expect { git_service.validate_for_hotfix! }.
        to output("Checking for commited files\nChecking if master and develop are up to date\nChecking for existing hotfix branch\n").to_stdout
    end

    it "does not raise an error when repository state is valid" do
      allow(git_repository).to receive(:has_added_files?).and_return(false)
      allow(git_repository).to receive(:develop_up_to_date?).and_return(true)
      allow(git_repository).to receive(:master_up_to_date?).and_return(true)
      allow(git_repository).to receive(:has_existing_branch_name?).and_return(false)

      expect { git_service.validate_for_hotfix! }.to_not raise_error
    end

    it "raises a GitError when there are staged files" do
      allow(git_repository).to receive(:develop_up_to_date?).and_return(true)
      allow(git_repository).to receive(:master_up_to_date?).and_return(true)
      allow(git_repository).to receive(:has_existing_branch_name?).and_return(false)

      expect(git_repository).to receive(:has_added_files?).and_return(true)

      expect { git_service.validate_for_hotfix! }.to raise_error(GitError)
    end

    it "raises a GitError when develop is not up to date with origin" do
      allow(git_repository).to receive(:has_added_files?).and_return(false)
      allow(git_repository).to receive(:master_up_to_date?).and_return(true)
      allow(git_repository).to receive(:has_existing_branch_name?).and_return(false)

      expect(git_repository).to receive(:develop_up_to_date?).and_return(false)

      expect { git_service.validate_for_hotfix! }.to raise_error(GitError)
    end

    it "raises a GitError when master is not up to date with origin" do
      allow(git_repository).to receive(:has_added_files?).and_return(false)
      allow(git_repository).to receive(:develop_up_to_date?).and_return(true)
      allow(git_repository).to receive(:has_existing_branch_name?).and_return(false)

      expect(git_repository).to receive(:master_up_to_date?).and_return(false)

      expect { git_service.validate_for_hotfix! }.to raise_error(GitError)
    end

    it "raises a GitFlowError when user already has another hotfix branch" do
      allow(git_repository).to receive(:has_added_files?).and_return(false)
      allow(git_repository).to receive(:develop_up_to_date?).and_return(true)
      allow(git_repository).to receive(:master_up_to_date?).and_return(true)

      expect(git_repository).
        to receive(:has_existing_branch_name?).
        with("hotfix").
        and_return(true)

      expect { git_service.validate_for_hotfix! }.to raise_error(GitFlowError)
    end
  end
end
