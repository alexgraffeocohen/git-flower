require "spec_helper"
require "tracker_api"

RSpec.describe GitFlower::PivotalProject do
  let(:tracker_client_mock) { instance_double("TrackerApi::Client") }
  let(:tracker_project_mock) { instance_double("TrackerApi::Resources::Project") }
  let(:token) { 123 }
  let(:project_id) { 456 }

  context "#initialize" do
    it "sets ups Pivotal API client with token" do
      allow(tracker_client_mock).
        to receive(:project)

      expect(TrackerApi::Client).
        to receive(:new).
        with(token: token).
        and_return(tracker_client_mock)

      GitFlower::PivotalProject.new(token, project_id)
    end

    it "sets up Privotal API project with id" do
      allow(TrackerApi::Client).
        to receive(:new).
        and_return(tracker_client_mock)

      expect(tracker_client_mock).
        to receive(:project).
        with(project_id)

      GitFlower::PivotalProject.new(token, project_id)
    end
  end

  context "#create_story" do
    let(:subject) { GitFlower::PivotalProject.new(token, project_id) }

    before do
      allow(TrackerApi::Client).
        to receive(:new).
        and_return(tracker_client_mock)
      allow(tracker_client_mock).
        to receive(:project).
        and_return(tracker_project_mock)
    end

    it "creates a hotfix when it receives hotfix type" do
      # TODO: extract service for finding users so I don't have to stub subject
      # under test.
      allow(subject).
        to receive(:find_users).
        and_return([])

      expect(tracker_project_mock).
        to receive(:create_story).
        with(
          name: "Cat Saving Plan",
          labels: ["urgent"],
          story_type: 'bug',
          current_state: 'started',
          owner_ids: []
      )

      subject.create_story("hotfix", {
        name: "Cat Saving Plan",
        labels: ["urgent"],
      })
    end

    it "creates a feature when it receives feature type" do
      # TODO: extract service for finding users so I don't have to stub subject
      # under test.
      allow(subject).
        to receive(:find_users).
        and_return([])

      expect(tracker_project_mock).
        to receive(:create_story).
        with(
          name: "Save all the Cats",
          labels: ["life-goal"],
          story_type: 'feature',
          current_state: 'started',
          estimate: 0,
          owner_ids: []
      )

      subject.create_story("feature", {
        name: "Save all the Cats",
        labels: ["life-goal"],
      })
    end
  end
end
