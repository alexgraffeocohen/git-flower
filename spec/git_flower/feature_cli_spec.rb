require "spec_helper"

RSpec.describe GitFlower::FeatureCLI do
  context "#create" do
    let(:feature_mock) {
      instance_double("GitFlower::Feature")
    }

    it "initializes a Feature and saves it" do
      allow(GitFlower::Feature).
        to receive(:new).
        and_return(feature_mock)
      expect(feature_mock).to receive(:save!)

      GitFlower::FeatureCLI.new.create("Story Name")
    end

    it "initializes Feature with story name" do
      expect(GitFlower::Feature).
        to receive(:new).
        with(story_name: "Story Name", labels: nil).
        and_return(feature_mock)
      allow(feature_mock).to receive(:save!)

      GitFlower::FeatureCLI.new.create("Story Name")
    end

    it "can initialize Feature with labels" do
      expect(GitFlower::Feature).
        to receive(:new).
        with(story_name: "Story Name", labels: ["cat"]).
        and_return(feature_mock)
      allow(feature_mock).to receive(:save!)

      cli = GitFlower::FeatureCLI.new
      cli.options = { labels: ["cat"] }
      cli.create("Story Name")
    end
  end
end
