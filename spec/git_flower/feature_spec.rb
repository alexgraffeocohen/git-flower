require 'spec_helper'

RSpec.describe GitFlower::Feature do
  let(:git_service_mock) { instance_double('GitFlower::GitService') }
  before do
    allow(GitFlower::GitService).
      to receive(:new).
      and_return(git_service_mock)
  end

  describe "#save!" do
    context "when story is valid" do
      let(:story) {
        GitFlower::Feature.new(story_name: 'Story', labels: [])
      }

      before do
        # Provide default message for any other ENV access that may occur
        allow(ENV).to receive(:[]) { nil }

        allow(ENV).
          to receive(:[]).
          with("PIVOTAL_TOKEN").
          and_return("token")
        allow(ENV).
          to receive(:[]).
          with("PIVOTAL_PROJECT_ID").
          and_return("id")
        allow(git_service_mock).
          to receive(:validate_for_feature!)
      end

      it "creates the story" do
        # TODO: Rather than stub the subject under test, we should transform
        # Story into a proper service instead of having it be a parent class
        # for Feature.
        expect(story).to receive(:create_story!)

        story.save!
      end
    end

    context "when story is invalid" do
      context "because of missing story name" do
        before do
          # Provide default message for any other ENV access that may occur
          allow(ENV).to receive(:[]) { nil }

          allow(git_service_mock).
            to receive(:validate_for_feature!)
          allow(ENV).
            to receive(:[]).
            with("PIVOTAL_TOKEN").
            and_return("token")
          allow(ENV).
            to receive(:[]).
            with("PIVOTAL_PROJECT_ID").
            and_return("id")
        end

        it "raises Argument error if nil" do
          story = GitFlower::Feature.new(story_name: nil, labels: [])

          # TODO: Rather than stub the subject under test, we should transform
          # Story into a proper service instead of having it be a parent class
          # for Feature.
          expect(story).to_not receive(:create_story!)
          expect { story.save! }.to raise_error(ArgumentError)
        end

        it "raises Argument error if empty" do
          story = GitFlower::Feature.new(story_name: "", labels: [])

          # TODO: Rather than stub the subject under test, we should transform
          # Story into a proper service instead of having it be a parent class
          # for Feature.
          expect(story).to_not receive(:create_story!)
          expect { story.save! }.to raise_error(ArgumentError)
        end
      end

      context "because of missing environment variables" do
        before do
          allow(git_service_mock).
            to receive(:validate_for_feature!)

          # Provide default message for any other ENV access that may occur
          allow(ENV).to receive(:[]) { nil }
        end

        context "pivotal token" do
          it "raises Argument error if nil" do
            allow(ENV).
              to receive(:[]).
              with("PIVOTAL_TOKEN").
              and_return(nil)

            story = GitFlower::Feature.new(story_name: "Story", labels: [])

            # TODO: Rather than stub the subject under test, we should transform
            # Story into a proper service instead of having it be a parent class
            # for Feature.
            expect(story).to_not receive(:create_story!)
            expect { story.save! }.to raise_error(ArgumentError)
          end

          it "raises Argument error if empty" do
            allow(ENV).
              to receive(:[]).
              with("PIVOTAL_TOKEN").
              and_return("")

            story = GitFlower::Feature.new(story_name: "Story", labels: [])

            # TODO: Rather than stub the subject under test, we should transform
            # Story into a proper service instead of having it be a parent class
            # for Feature.
            expect(story).to_not receive(:create_story!)
            expect { story.save! }.to raise_error(ArgumentError)
          end
        end

        context "pivotal project id" do
          it "raises Argument error if nil" do
            allow(ENV).
              to receive(:[]).
              with("PIVOTAL_PROJECT_ID").
              and_return(nil)

            story = GitFlower::Feature.new(story_name: "Story", labels: [])

            # TODO: Rather than stub the subject under test, we should transform
            # Story into a proper service instead of having it be a parent class
            # for Feature.
            expect(story).to_not receive(:create_story!)
            expect { story.save! }.to raise_error(ArgumentError)
          end

          it "raises Argument error if empty" do
            allow(ENV).
              to receive(:[]).
              with("PIVOTAL_PROJECT_ID").
              and_return("")

            story = GitFlower::Feature.new(story_name: "Story", labels: [])

            # TODO: Rather than stub the subject under test, we should transform
            # Story into a proper service instead of having it be a parent class
            # for Feature.
            expect(story).to_not receive(:create_story!)
            expect { story.save! }.to raise_error(ArgumentError)
          end
        end
      end
    end
  end
end
