require "spec_helper"

RSpec.describe GitFlower::Branch do
  it "can create a feature branch name" do
    branch = GitFlower::Branch.new(
      name: "Save The Cats",
      id: 1,
      type: "feature"
    )

    expect(branch.name).to eq("feature/1-save-the-cats")
  end

  it "can create a hotfix branch name" do
    branch = GitFlower::Branch.new(
      name: "Lost a Cat",
      id: 1,
      type: "hotfix"
    )

    expect(branch.name).to eq("hotfix/1-lost-a-cat")
  end
end
