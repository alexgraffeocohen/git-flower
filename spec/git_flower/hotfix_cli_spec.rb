require "spec_helper"

RSpec.describe GitFlower::HotfixCLI do
  context "#create" do
    let(:hotfix_mock) {
      instance_double("GitFlower::Hotfix")
    }

    it "initializes a Hotfix and saves it" do
      allow(GitFlower::Hotfix).
        to receive(:new).
        and_return(hotfix_mock)
      expect(hotfix_mock).to receive(:save!)

      GitFlower::HotfixCLI.new.create("Story Name")
    end

    it "initializes Hotfix with story name" do
      expect(GitFlower::Hotfix).
        to receive(:new).
        with(story_name: "Story Name", labels: nil).
        and_return(hotfix_mock)
      allow(hotfix_mock).to receive(:save!)

      GitFlower::HotfixCLI.new.create("Story Name")
    end

    it "can initialize Hotfix with labels" do
      expect(GitFlower::Hotfix).
        to receive(:new).
        with(story_name: "Story Name", labels: ["cat"]).
        and_return(hotfix_mock)
      allow(hotfix_mock).to receive(:save!)

      cli = GitFlower::HotfixCLI.new
      cli.options = { labels: ["cat"] }
      cli.create("Story Name")
    end
  end
end
