require "spec_helper"

RSpec.describe GitFlower::Story do
  let(:pivotal_mock) { instance_double("GitFlower::PivotalProject") }
  let(:pivotal_story_mock) { instance_double("TrackerApi::Resources::Story") }
  let(:git_service_mock) { instance_double("GitFlower::GitService") }
  let(:pivotal_story_id) { 1 }

  before do
    allow(pivotal_mock).
      to receive(:create_story).
      and_return(pivotal_story_mock)
    allow(pivotal_story_mock).
      to receive_messages(id: pivotal_story_id)
    allow(GitFlower::GitService).
      to receive(:new).
      and_return(git_service_mock)
  end

  context "integration with Hitch" do
    it "does not raise an exception without hitch file" do
      allow(GitFlower::PivotalProject).
        to receive(:new).
        and_return(pivotal_mock)
      allow(git_service_mock).
        to receive(:start_branch)

      story = GitFlower::Story.new(
        story_name: "Save All The Cats",
        labels: []
      )

      expect { story.create_story!("feature") }.
        not_to raise_error
    end
  end

  context "initializing Pivotal Project" do
    let(:project_id) { "123" }
    let(:token) { "456" }

    before do
      allow(ENV).
        to receive(:[]).
        with("PIVOTAL_PROJECT_ID").
        and_return(project_id)
      allow(ENV).
        to receive(:[]).
        with("PIVOTAL_TOKEN").
        and_return(token)
      allow(ENV).
        to receive(:[]).
        with("HOME")
      allow(ENV).
        to receive(:[]).
        with("USER")
    end

    it "uses user-set env variables" do
      expect(GitFlower::PivotalProject).
        to receive(:new).
        with(token, project_id).
        and_return(pivotal_mock)
      allow(git_service_mock).
        to receive(:start_branch)

      story = GitFlower::Story.new(
        story_name: "Save All The Cats",
        labels: []
      )
      story.create_story!("feature")
    end
  end

  context "#create_story!" do
    let(:story_name) { "Save All The Cats" }
    let(:story) {
      GitFlower::Story.new(story_name: story_name, labels: [])
    }

    it "raises an ArgumentError if it receives unsupported type" do
      expect {
        story.create_story!('cat-feature')
      }.to raise_error(ArgumentError)
    end

    context "creating Features" do
      before do
        allow(GitFlower::PivotalProject).
          to receive(:new).
          and_return(pivotal_mock)
      end

      it "creates a feature branch" do
        expect(git_service_mock).
          to receive(:start_branch).
          with(name: story_name, id: pivotal_story_id, type: "feature")

        story.create_story!("feature")
      end
    end

    context "creating Hotfixes" do
      before do
        allow(GitFlower::PivotalProject).
          to receive(:new).
          and_return(pivotal_mock)
      end

      it "creates a hotfix branch" do
        expect(git_service_mock).
          to receive(:start_branch).
          with(name: story_name, id: pivotal_story_id, type: "hotfix")

        story.create_story!("hotfix")
      end
    end
  end
end
