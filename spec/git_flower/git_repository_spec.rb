require "spec_helper"
require "git"
require 'byebug'

RSpec.describe GitFlower::GitRepository, fakefs: true do
  let(:git_mock) { instance_double("Git::Base") }
  let(:git_branch_mock) { instance_double("Git::Branch") }

  describe "#initialize" do
    context "there is a .git folder present in the directory where the CLI is run" do
      it "stashes the git repository" do
        Dir.mkdir(".git")
        expect(Git).to receive(:open).with(Dir.pwd)

        GitFlower::GitRepository.new(Dir.pwd)
      end
    end

    context "there is no .git folder in the directory where the CLI is run" do
      it "raises a GitError" do
        expect { GitFlower::GitRepository.new(Dir.pwd) }.to raise_error(GitError)
      end
    end
  end

  describe "#checkout_branch" do
    it "creates and checks out a branch with the given name" do
      branch_name = "Save the Cats"
      Dir.mkdir(".git")

      expect(Git).to receive(:open).and_return(git_mock)
      expect(git_mock).
        to receive(:branch).
        with(branch_name).
        and_return(git_branch_mock)
      expect(git_branch_mock).to receive(:checkout)

      GitFlower::GitRepository.
        new(Dir.pwd).
        checkout_branch(branch_name)
    end
  end
end
